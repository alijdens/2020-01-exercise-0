package ar.uba.fi.tdd.exercise;

import static org.assertj.core.api.Assertions.assertThat;

import ar.uba.fi.tdd.exercise.item.AgedBrie;
import ar.uba.fi.tdd.exercise.item.BackstagePass;
import ar.uba.fi.tdd.exercise.item.Conjured;
import ar.uba.fi.tdd.exercise.item.GenericItem;
import ar.uba.fi.tdd.exercise.item.Sulfuras;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
class GildedRoseTest {

    @Test
    public void foo() {
        UpdatableItem[] items = new UpdatableItem[] { new GenericItem("fixme", 0, 0) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(app.items[0].Name).isEqualTo("fixme");
    }

    @Test
    public void sellInDateIsDecreasedEachDay() {
        final int SELL_IN_DAYS_ITEM_1 = 5;
        final int SELL_IN_DAYS_ITEM_2 = 3;

        UpdatableItem[] items = new UpdatableItem[] {
            new GenericItem("test1", SELL_IN_DAYS_ITEM_1, 0),
            new GenericItem("test2", SELL_IN_DAYS_ITEM_2, 0)
        };
        GildedRose app = new GildedRose(items);

        for (int i = 0; i < 5; i++) {
            app.updateQuality();

            assertThat(app.items[0].sellIn).isEqualTo(SELL_IN_DAYS_ITEM_1 - i - 1);
            assertThat(app.items[1].sellIn).isEqualTo(SELL_IN_DAYS_ITEM_2 - i - 1);
        }
    }

    @Test
    public void qualityDecreasesOverTimeForNormalItems() {
        UpdatableItem[] items = new UpdatableItem[] {
            new GenericItem("normal_item_1", 5, 10),
            new GenericItem("normal_item_2", 3, 2)
        };
        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(9);
        assertThat(app.items[1].quality).isEqualTo(1);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(8);
        assertThat(app.items[1].quality).isEqualTo(0);
    }

    @Test
    public void qualityCannotBeNegative() {
        UpdatableItem[] items = new UpdatableItem[] {
            new GenericItem("normal_item_1", 5, 1),
            new GenericItem("normal_item_2", 3, 2)
        };
        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(0);
        assertThat(app.items[1].quality).isEqualTo(1);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(0);
        assertThat(app.items[1].quality).isEqualTo(0);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(0);
        assertThat(app.items[1].quality).isEqualTo(0);
    }

    @Test
    public void onceTheSellByDateHasPassedQualityDegradesTwiceAsFast() {
        UpdatableItem[] items = new UpdatableItem[] { new GenericItem("normal_item_1", 1, 6) };
        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertThat(app.items[0].sellIn).isEqualTo(0);
        assertThat(app.items[0].quality).isEqualTo(5);

        app.updateQuality();
        assertThat(app.items[0].sellIn).isEqualTo(-1);
        assertThat(app.items[0].quality).isEqualTo(3);

        app.updateQuality();
        assertThat(app.items[0].sellIn).isEqualTo(-2);
        assertThat(app.items[0].quality).isEqualTo(1);

        app.updateQuality();
        assertThat(app.items[0].sellIn).isEqualTo(-3);
        assertThat(app.items[0].quality).isEqualTo(0);
    }

    @Test
    public void agedBrieIncreasesQualityTheOlderItGets() {
        UpdatableItem[] items = new UpdatableItem[] { new AgedBrie(5, 10) };
        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(11);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(12);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(13);
    }

    @Test
    public void itemQualityCannotBeOver50() {
        UpdatableItem[] items = new UpdatableItem[] { new AgedBrie(5, 48) };
        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(49);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(50);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(50);
    }

    @Test
    public void sulfurasQualityDoesntDecrease() {
        UpdatableItem[] items = new UpdatableItem[] { new Sulfuras(5) };
        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(80);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(80);

        assertThat(app.items[0].sellIn).isEqualTo(5);
    }

    @Test
    public void backstageIncreasesInQualityAsItsSellInValueApproaches() {
        UpdatableItem[] items = new UpdatableItem[] { new BackstagePass(15, 25) };
        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(26);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(27);
    }

    @Test
    public void backstageIncreasesQualityBy2WhenThereAre10OrLessDaysToSellIn() {
        UpdatableItem[] items = new UpdatableItem[] { new BackstagePass(11, 30) };
        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(31);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(33);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(35);
    }

    @Test
    public void backstageIncreasesQualityBy3WhenThereAre5OrLessDaysToSellIn() {
        UpdatableItem[] items = new UpdatableItem[] { new BackstagePass(6, 10) };
        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(12);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(15);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(18);
    }

    @Test
    public void backstageQualityDropsTo0WhenExpired() {
        UpdatableItem[] items = new UpdatableItem[] { new BackstagePass(2, 10) };
        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(13);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(16);

        // at this point, the sell in date has been reached, so quality drops
        // to zero
        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(0);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(0);
    }

    @Test
    public void conjuredItem() {
        UpdatableItem[] items = new UpdatableItem[] { new Conjured("conjured", 2, 10) };
        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(8);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(6);

        // at this point, the sell in date has been reached, so the quality
        // degrades at double speed
        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(2);

        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(0);
    }
}
