package ar.uba.fi.tdd.exercise.item;

import ar.uba.fi.tdd.exercise.UpdatableItem;

public class GenericItem extends UpdatableItem {

    public GenericItem(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    @Override
    public int getQualityChange() {
        return (this.sellIn > 0) ? -1 : -2;
    }
}
