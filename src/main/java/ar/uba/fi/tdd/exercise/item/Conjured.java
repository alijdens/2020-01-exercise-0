package ar.uba.fi.tdd.exercise.item;

public class Conjured extends GenericItem {
    
    public Conjured(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    @Override
    public int getQualityChange() {
        // conjured items degrade twice as fast as normal items
        return super.getQualityChange() * 2;
    }
}
