package ar.uba.fi.tdd.exercise.item;

import ar.uba.fi.tdd.exercise.UpdatableItem;

public class BackstagePass extends UpdatableItem {

    public BackstagePass(int sellIn, int quality) {
        super("Backstage passes to a TAFKAL80ETC concert", sellIn, quality);
    }

    @Override
    public int getQualityChange() {
        if (this.sellIn <= 0) {
            // when sell in date is reached, quality turns zero
            return -this.quality;
        } else if (this.sellIn <= 5) {
            return 3;
        } else if (this.sellIn <= 10) {
            return 2;
        } else {
            return 1;
        }
    }
}
