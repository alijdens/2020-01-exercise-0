package ar.uba.fi.tdd.exercise;

public abstract class UpdatableItem extends Item {
    
    public UpdatableItem(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    // Returns the change in the item's quality that the item would suffer
    // if a day passed. 
    public abstract int getQualityChange();
    
    // Updates the item's quality based on the sell in days.
    public void updateQuality() {
        this.quality += this.getQualityChange();
        this.quality = Math.max(0, this.quality);

        this.sellIn -= 1;
    }
}
