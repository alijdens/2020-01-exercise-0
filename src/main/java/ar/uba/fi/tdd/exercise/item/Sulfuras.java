package ar.uba.fi.tdd.exercise.item;

import ar.uba.fi.tdd.exercise.UpdatableItem;

public class Sulfuras extends UpdatableItem {
    
    public Sulfuras(int sellIn) {
        // Sulfuras always has 80 quality.
        super("Sulfuras, Hand of Ragnaros", sellIn, 80);
    }

    @Override
    public int getQualityChange() {
        // Sulfuras is a legendary item, so its quality is never altered.
        return 0;
    }

    @Override
    public void updateQuality() {
        // Sulfuras is a legendary item, so its quality is never altered.
    }
}
