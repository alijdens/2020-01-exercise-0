
package ar.uba.fi.tdd.exercise;

class GildedRose {
    UpdatableItem[] items;

    public GildedRose(UpdatableItem[] items) {
        this.items = items;
    }

    // Updates the items quality based on their type and the sell in days.
    public void updateQuality() {
        for (UpdatableItem item : items) {
            item.updateQuality();
        }
    }
}
