package ar.uba.fi.tdd.exercise.item;

import ar.uba.fi.tdd.exercise.UpdatableItem;

public class AgedBrie extends UpdatableItem {
    
    public AgedBrie(int sellIn, int quality) {
        super("Aged Brie", sellIn, quality);
    }

    @Override
    public int getQualityChange() {
        if (this.quality == 50) {
            return 0;
        }

        return 1;
    }

}
